Properties of Interactive Agents
================================

Interactive
    Must react to player's behavior

Challenging
    Must adapt it's behavior to fit a player's skill

Adaptative
    The agent must provide an behavior to complete a task independently of its situation. For the same input, the output may be different if the status of the agent is.

Restricted
    The agent inputs, planning and outputs are restricted by the rules of the game's world


