#!/bin/bash

AS_ROOT="sudo " 
INSTALL="apt-get install "
PYTHON3_BASE_PKGS="python3.4 python3.4-venv python3-dev"
PYTHON3_SCI_PKGS="python3-numpy python3-scipy python3-pil"
PYTHON3_SCIPY_SPECIFIC_PKGS="libblas-dev liblapack-dev libatlas-base-dev gfortran"
PILLOW_SPECIFIC_PKGS="libjpeg-dev zlib1g-dev"

# Install required packages on a Ubuntu/Debian based distro
$AS_ROOT $INSTALL $PYTHON3_BASE_PKGS $PYTHON3_SCI_PKGS $PYTHON3_SCIPY_SPECIFIC_PKGS


#Removing previous installation of pmgai
rm -Rf pmgai
mkdir pmgai
cd pmgai
python3 -m venv pyvenv
source pyvenv/bin/activate

# Optional when bug will be fixed
python -m pip install pip --ignore-installed

pip install nuclai


# Installing placeholder
nuclai install placeholder


# Installing lab#1
nuclai install hal9000
