Racing Driver
=============

* Input Information

	- Velocity
	- Position
    - Visible part of the track
    - Position of opponent
    - Rank in the race
    - Inertia of the vehicule

* Output Generated

	- Acceleration (Forward/Brake)
	- Direction

----

Card Game Opponent
==================

* Input Information

	- Own deck's content
	- Board status
    - History of moves
    - Turn count
    - Turn time

* Output Generated

	- Next move (card(s) played and position)
	- Plan for next N moves

----

Squad Leader
============

* Input Information

	- Squadmates position
	- Squadmates status
    - Own position
    - Own status
    - Objective position
    - Objective status
    - Terrain

* Output Generated

	- Orders to squadmates

