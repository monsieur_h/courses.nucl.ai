    Architecting HAL9000
    ====================

    ----

    Computation Phases
    ==================

    * Sense

        - Detect astronauts properties :
            - position
            - Activity
        - Listen to astronaut speech
        - Detect ships position/direction/status


    * Think

        - Describe the main decisions made.
        - Answer astronaut's questions
        - Decide wether following astronauts order or differ
        (The function to optimize is "Maximize probability of mission success")


    * Act
        
        - Describe what output needs to be generated.
        - Rotate ship
        - Accelerate ship
        - Open/close doors
        - Turn off/on lights
        - Turn on/off subsystems (communication, air conditionner...)

    ----

    Functional Layers
    =================

    Navigation Layer
        Navigates the ship to Jupiter

    Ship system Layer
        Operates differents ship system (lights, doors, propulsion, communication)

    Human communication Layer
        Vocalizes and recognizes text from sound
       
    ...
